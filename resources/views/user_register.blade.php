@extends('layout')
@section('content')
    <div class="col-md-12">
        <div class="row">
            <form id="register" name="register" method="post" action="{{ route('user_register_form') }}">
                @csrf
                <div class="form-group">
                    <input type="text" name="name" class="form-control" placeholder="Ad">
                    <input type="text" name="email" class="form-control" placeholder="Email">
                    <input type="password" name="password" class="form-control" placeholder="Şifrə">
                    <br/>
                    <button type="submit">Qeydiyyat</button>
                </div>
            </form>
        </div>
    </div>
@endsection
