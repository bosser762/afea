@extends('layout')
@section('content')
    <div class="col-md-12">
        <div class="row">
            <form id="register" name="register" method="post" action="{{ route('create_post_form') }}" enctype="multipart/form-data">
                @csrf
                <div class="form-group">
                    <input type="text" name="title" class="form-control" placeholder="Title">
                    <textarea name="content" id="" cols="30" rows="10" class="form-control">Content</textarea>
                    <input type="text" name="tags" class="form-control" placeholder="tags">
                    <input type="file" name="image" class="form-control">
                    <br/>
                    <button type="submit">Send</button>
                </div>
            </form>
        </div>
    </div>
@endsection
