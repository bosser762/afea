@extends('layout')
@section('content')
    <div class="col-md-12">
        <div class="row">
            <form id="login" name="login" method="post" action="{{ route('user_login_form') }}">
                @csrf
                <div class="form-group">
                    <input type="text" name="email" class="form-control" placeholder="Email">
                    <input type="password" name="password" class="form-control" placeholder="Şifrə">
                    <br/>
                    <button type="submit">Daxil ol</button>
                </div>
            </form>
        </div>
    </div>
@endsection
