@extends('layout')
@section('content')
    <ul>
        <li><a href="{{ route('posts') }}">Posts</a></li>
        <li><a href="{{ route('post_create') }}">Create</a></li>
    </ul>
   <div class="container">
       <div class="row">
           @foreach($posts as $post)
               <div class="col-md-12">
                   <h2>{{$post->title}}</h2>
                   <p>{{$post->tags}}</p>
                   <p>{{$post->content}}</p>
                   <img src="{{URL::asset('public/images').$post->image}}" alt="" width="200px" height="100px">
                   <p>
                       <a href="{{route('delete_post',$post->id)}}">Delete Post</a>
                   </p>
               </div>
           @endforeach
       </div>
   </div>
@endsection
