<?php

use App\Http\Controllers\Controller;
use App\Http\Controllers\PostsController;
use App\Http\Controllers\UserController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    $message = false;
    return view('welcome');
})->name('home');


//Route::group(['prefix' => 'admin'], function () {
//    Voyager::routes();
//});



Route::get('/register',[UserController::class,'create'])->name('user_register');
Route::get('/post/create',[PostsController::class,'create'])->name('post_create');
Route::get('/post/posts',[PostsController::class,'index'])->name('posts');
Route::get('/login',[UserController::class,'index'])->name('user_login');
Route::get('/dashboard',[Controller::class,'index'])->name('dashboard')->middleware('auth');
Route::post('/register/form',[UserController::class,'store'])->name('user_register_form');
Route::post('/login/form',[UserController::class,'login'])->name('user_login_form');
Route::post('/post/add',[PostsController::class,'store'])->name('create_post_form');
Route::get('/post/delete/{id}',[PostsController::class,'destroy'])->name('delete_post');
