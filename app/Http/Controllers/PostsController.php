<?php

namespace App\Http\Controllers;

use App\Models\Post;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;

class PostsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $posts = Post::where('User_id','=',auth()->user()->getAuthIdentifier())->get();
        return view('posts',compact('posts'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('create_post');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $post = new Post();
        $request->validate([
            'image' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
        ]);
        $request = $request->except('_token');
        $post->title = $request['title'];
        $post->content = $request['content'];
        $post->tags = $request['tags'];
        $post->user_id = auth()->user()->getAuthIdentifier();


        $imageName = time().'.'.$request['image']->extension();
        $post->image = $imageName;
        $post->save();
        $request['image']->move(public_path('images'), $imageName);
        return new RedirectResponse(route('dashboard'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $post = new Post();
        $request->validate([
            'image' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
        ]);
        $request = $request->except('_token');
        $post->title = $request['title'];
        $post->content = $request['content'];
        $post->tags = $request['tags'];
        $post->user_id = auth()->user()->getAuthIdentifier();


        $imageName = time().'.'.$request['image']->extension();
        $post->image = $imageName;
        $post->save();
        $request['image']->move(public_path('images'), $imageName);
        return new \Symfony\Component\HttpFoundation\RedirectResponse(route('dashboard'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $post = Post::find($id);
        $post->delete();
        return redirect(route('dashboard'));
    }
}
